const gulp = require('gulp');
const livereload = require('gulp-livereload');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('default', function() {
  gulp.src('css/*.css')
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('dist'))
});

gulp.task('watch', function(){
  livereload.listen();
  gulp.watch('css/*.css', function(file){
    livereload.changed(file)
  });
})
